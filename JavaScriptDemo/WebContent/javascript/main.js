/**
 * 
 */
//document.getElementById('issueForm').addEventListener('submit', addIssue());
function addIssue() {
	var issueDesc = document.getElementById('issueDescInput').value;
	console.log(issueDesc);
	var issueSeverity = document.getElementById('issueSeverityInput').value;
	var issueAssignee = document.getElementById('issueResInput').value;
	var issueStatus = 'Open';
	var issueId = chance.guid();

	var issue = {
		id : issueId,
		description : issueDesc,
		severity : issueSeverity,
		assignee : issueAssignee,
		status : issueStatus
	}
	if (localStorage.getItem('issues') == null) {
		var issues = [];
		issues.push(issue);
		localStorage.setItem('issues', JSON.stringify(issues));
	} else {
		var issues = JSON.parse(localStorage.getItem('issues'));
		issues.push(issue);
		localStorage.setItem('issues', JSON.stringify(issues));
	}
	document.getElementById('issueForm').reset();
	fetchIssues();
	e.preventDefault();
}

function close_Issue(id) {
	console.log("close1");
	var closeIssues = JSON.parse(localStorage.getItem('issues'));
	console.log(closeIssues[0]);
	for (var i = 0; i < closeIssues.length; i++) {
		console.log('2');
		if (closeIssues[i].id == id) {
			console.log('3');
			closeIssues[i].status = 'Closed';
			//issues.splice(i, 1);

		}
	}
	console.log('4');
	localStorage.setItem('issues', JSON.stringify(closeIssues));
	fetchIssues();
}
function delete_Issue(id) {
	console.log("close1");
	var closeIssues = JSON.parse(localStorage.getItem('issues'));
	console.log(closeIssues[0]);
	for (var i = 0; i < closeIssues.length; i++) {
		console.log('2');
		if (closeIssues[i].id == id) {
			console.log('3');
			//closeIssues[i].status = 'Closed';
			closeIssues.splice(i, 1);

		}
	}
	console.log('4');
	localStorage.setItem('issues', JSON.stringify(closeIssues));
	fetchIssues();
}

function fetchIssues() {
	console.log("check-1");
	var issues = JSON.parse(localStorage.getItem('issues'));
	var issueList = document.getElementById('issueList');
	issueList.innerHTML = "";
	console.log(issues);
	if (issues) {
		console.log("check-3");
		for (var i = 0; i < issues.length; i++) {
			console.log("check-4");
			var id = issues[i].id;
			var desc = issues[i].description;
			var severity = issues[i].severity;
			var assignee = issues[i].assignee;
			var status = issues[i].status;

			issueList.innerHTML += '<div class="card bg-light">' +
				'<div class="card-body">' +
				'<h6 class="card-title">Issue Id:' + id + '</h6>' +
				'<p><span class="badge badge-pill badge-info">' + status + '</span></p>' +
				'<h3 class="card-text">' + desc + '</h3>' +
				'<p><span class="fa fa-exclamation-triangle" aria-hidden="true"></span>&nbsp' + severity + '</p>' +
				'<p><span class="fa fa-user" aria-hidden="true"></span>&nbsp' + assignee + '</p>' +
				'<button onclick="close_Issue(\'' + id + '\')" class="btn btn-warning">Close</button> ' +
				' <button onclick="delete_Issue(\'' + id + '\')" class="btn btn-danger">Delete</button> ' +
				'</div> </div>';
		}

	}
}