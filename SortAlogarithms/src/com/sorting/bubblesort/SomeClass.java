package com.sorting.bubblesort;
import java.util.concurrent.ThreadFactory;

public class SomeClass implements Runnable {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	public static void main(){
		someRandomClass sc = new someRandomClass();
		Thread thread1 = new Thread() {
			   public void run() {
			     sc.methodOne();
			   }
			};
			Thread thread2 = new Thread() {
			   public void run() {
				   sc.methodTwo();
			   }
			};
//			Thread thread3 = new Thread() {
//			   public void run() {
//			      Utils.insertLog();
//			   }
//			};
			thread1.start();
			thread2.start();
			//thread3.start();
	}
}

class someRandomClass{
	
	public synchronized void methodOne() {

		for (int i = 0; i < 4; i++) {
			System.out.println("In method One I am Synchronized");

		}
	}
	

	public void methodTwo() {

		for (int i = 0; i < 4; i++) {
			System.out.println("In method Two I am Synchronized");

		}
	}

}
