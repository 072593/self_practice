package com.sorting.bubblesort;

public class BubbleSortTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] input = { 4, 2, 9, 6, 23, 12,0,-5,34, 34, 0, 1 };
		BubbleSortTest.bubbleSorting(input);

	}
	
	public static void bubbleSorting(int [] inputArray){
		
		int n = inputArray.length;
		for(int i= 0; i<=n-1;i++){
			
			for(int j = 0;j<n-1;j++){
				if(inputArray[j]>inputArray[j+1]){
					
					swap(j,j+1,inputArray);
				}
			}
		}
		printArray(inputArray);
	}
	
	public static void swap(int m, int p, int [] array){
		
		array[m] = array[m] + array[p];
		array[p] = array[m] - array[p];
		array[m] = array[m] - array[p];
	}
	
	public static void printArray(int [] inputArray){
		for(int n = 0; n<=inputArray.length-1;n++){
			
			System.out.println(inputArray[n]);
		}
	}

}
