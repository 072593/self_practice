package com.java.singleton.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.java.singleton.Singleton;

public class SingletonReflectionTest {

	public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, SecurityException, Exception, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class claz = Class.forName("com.java.singleton.Singleton");
		Constructor<Singleton> con = claz.getDeclaredConstructor();
		con.setAccessible(true);
		Singleton s3 = con.newInstance();
		System.out.println("s3"+s3);
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println("s1"+s1);
		System.out.println("s2"+s2);

	}

}
