package com.java.singleton.test;

import java.lang.reflect.InvocationTargetException;

import com.java.singleton.Singleton;

public class SingletonCloneTest {

	public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, SecurityException, Exception, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println("s1"+s1);
		System.out.println("s2"+s2);
	Singleton s3 = (Singleton) s2.clone();
	System.out.println("s3"+s3);
	}
}
