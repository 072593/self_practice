package com.java.singleton.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import com.java.singleton.Singleton;

public class SingletonSerialDeserialTest {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println("s1"+s1);
		System.out.println("s2"+s2);
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("C:\\Users\\jaswa\\Desktop\\Downloads\\s2.ser")); 
		oos.writeObject(s2);
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("C:\\Users\\jaswa\\Desktop\\Downloads\\s2.ser"));
		Singleton s3 = (Singleton) ois.readObject();
		System.out.println("s3"+s3);
		
	}
		

}
