package com.java.singleton;

public class SingletonBestApproch {

		private static SingletonBestApproch INSTANCE = null; 
		//only single class have access to it
		private  SingletonBestApproch() {
			// TODO Auto-generated constructor stub
			if(INSTANCE !=null){
				 throw new RuntimeException("Cannot create new instance again");
			}
		} 
		//directly giving the single instance that we created
		public static SingletonBestApproch getInstance(){
			if(INSTANCE == null){
				INSTANCE = new SingletonBestApproch();
				}
			return INSTANCE;
		}
}
