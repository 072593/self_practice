package com.java.singleton;

import java.io.Serializable;

public class Singleton implements Serializable, Cloneable  {

	//Eager intialization 
	private static Singleton INSTANCE = new Singleton();
	//only single class have access to it
	private  Singleton(){
		if(INSTANCE !=null){
			 throw new RuntimeException("Cannot create new instance again");
		}
	} 
	//directly giving the single instance that we created
	public static Singleton getInstance(){
		if(INSTANCE == null){
			INSTANCE = new Singleton();
			}
		return INSTANCE;
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
