# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 1.0
* This Repository shows my work on Angularjs,Javascript,Restful Web Services, Sort Alogarithms, Singleton Design Pattern, Bootstrap, CSS3, HTML5.

### This Repo contains different projects ###

* Messenger - Used jax-rs to produce rest endpoints.
* SortAlogorithms - worked alogarithms to get hands on experience.
* Saibersys_Assignment - is Weather report App which consumes weatther api in Angularjs, the UI looks rich because I used Bootstrap 4.0,CSS3,HTML5.  
* Multithreading-Handson - to gain insights in multithreading world self learning.
* Javascript_Demo - this is Issue Tracker app which will use browser local storage,Javascript,HTML5,CSS3,Bootstrap 4.0 this app helps in assigning issues to developer and help in track them.
* Experimenting_Singleton - this is self learning project to get handson experience and understand singleton.

### How do I get set up? ###
* For Angular project to work they need angularjs1.2 version and the rest of the projects works fine you can download and run in any IDE. 

### Who do I talk to? ###

* Repo owner or admin
Jashwanth ch