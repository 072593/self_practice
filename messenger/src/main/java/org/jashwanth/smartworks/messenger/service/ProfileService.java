package org.jashwanth.smartworks.messenger.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jashwanth.smartworks.messenger.database.DatabaseClass;
import org.jashwanth.smartworks.messenger.model.Profile;

public class ProfileService {
private Map<String,Profile> Profiles = DatabaseClass.getProfiles();
	
	public ProfileService(){
		
		Profile p1 = new Profile(1,"Prudhvi","Hello pru","Jashwanth");
		Profile p2 = new Profile(3,"Rakesh","Hello rakesh","rakesh");
		Profile p3 = new Profile(2,"Kalyan","Hello das","Kalyan");
		Profile p4 = new Profile(4,"Jashwanth","Na Savu Nen Sastha","Jashwanth");
	Profiles.put("Prudhvi", p1);	
	Profiles.put("Kalyan", p3);
	Profiles.put("Rakesh", p2);
	Profiles.put("Jashwanth", p4);
	}
	
	public List<Profile> getAllProfiles(){
		
		return  new ArrayList<Profile> (Profiles.values());
	
	}
	public Profile getProfile(String profileName){
		
		return Profiles.get(profileName);
	}
	public Profile addProfile(Profile Profile){
		
		Profile.setId(Profiles.size()+1);
		Profiles.put(Profile.getProfileName(), Profile);
		return Profile;
		
	}
	public Profile updateProfile(Profile profile){
		if(!profile.getProfileName().isEmpty()){
			System.out.println(profile.toString());
			Profiles.put(profile.getProfileName(), profile);
			return profile;
		}else{
			return null;
		}
		
	}
	public Profile removeProfile(String profileName){
		if(profileName != null){
			return Profiles.remove(profileName);
			 
		}else{
			return null;
		}
}
}
