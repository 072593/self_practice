package org.jashwanth.smartworks.messenger.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jashwanth.smartworks.messenger.model.Profile;
import org.jashwanth.smartworks.messenger.service.ProfileService;

@Path("/profiles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProfileResource {
	
	ProfileService ps = new ProfileService();
	@GET
	public List<Profile> getAllProfiles(){
		return ps.getAllProfiles();
	}
	@GET
	@Path("/{profileName}")
	public Profile getProfile(@PathParam("profileName") String proName){
		return ps.getProfile(proName);
	}
	@POST
	public Profile addProfile(Profile Profile){
		return ps.addProfile(Profile);
	}
	@PUT
	@Path("/{profileName}")
	public Profile updateProfile(@PathParam("profileName") String proName,Profile Profile){
		Profile.setProfileName(proName);
		return ps.updateProfile(Profile);
	}
	@DELETE
	@Path("/{profileName}")
	public void removeProfile(@PathParam("profileName") String proName){
		ps.removeProfile(proName);
	}
	

}
