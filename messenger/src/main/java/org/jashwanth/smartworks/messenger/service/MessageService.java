package org.jashwanth.smartworks.messenger.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jashwanth.smartworks.messenger.database.DatabaseClass;
import org.jashwanth.smartworks.messenger.model.Message;

public class MessageService {
	
	private Map<Integer,Message> messages = DatabaseClass.getMessages();
	
	public MessageService(){
		
		Message m1 = new Message(1,"Hello Rest","Jashwanth");
		Message m2 = new Message(12,"Hello Pru","Krishna");
		Message m3 = new Message(3,"Hello das","Kalyan");
		Message m4 = new Message(4,"Na Savu Nen Sastha","Jashwanth");
	messages.put(1, m1);	
	messages.put(2, m2);
	messages.put(3, m3);
	messages.put(4, m4);
	}
	
	public List<Message> getAllMessages(){
		
		return  new ArrayList<Message> (messages.values());
	
	}
	public Message getMessage(int id){
		
		return messages.get(id);
	}
	public Message addMessage(Message message){
		
		message.setId(messages.size()+1);
		messages.put(message.getId(), message);
		return message;
		
	}
	public Message updateMessage(Message message){
		if(message.getId()>=0){
			messages.put(message.getId(), message);
			return message;
		}else{
			return null;
		}
		
	}
	public Message removeMessage(int id){
		if(id>=0){
			return messages.remove(id);
			 
		}else{
			return null;
		}
		
		
		
		
	}
	
	

}
